package totoro.yui.actions

/**
 * Action classes annotated with this annotation will be hooked up at startup,
 * and registered as action processors automatically.
 * Works good for simple commands that do not need construction parameters.
 */

@Target(AnnotationTarget.CLASS)
annotation class Action
