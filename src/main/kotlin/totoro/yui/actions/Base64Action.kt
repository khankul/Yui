package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import java.nio.charset.StandardCharsets
import java.util.*

@Suppress("unused")
@Action
class Base64Action : SensitivityAction("base64", "base", "64", "b64") {
    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        if (command.args.isNotEmpty()) {
            if (command.modifier == "encode") {
                client.send(command.chan, encode(command.content))
                return true
            }
            else if (command.modifier == "decode") {
                client.send(command.chan, decode(command.args.first()))
                return true
            }

        }
        client.send(command.chan, "do you want to `~base64:decode xxx` or `~base64:encode xxx`?")
        return true
    }

    private fun encode(value: String): String {
        return Base64.getEncoder().encodeToString(value.toByteArray(StandardCharsets.UTF_8))
    }
    private fun decode(value: String): String {
        return Base64.getDecoder().decode(value).toString(StandardCharsets.UTF_8)
    }

    override val description = "Base64 encoding/decoding tool. Use :encode / :decode modifiers correspondingly. Example: ~b64:decode YXBwbGU="
}
