package totoro.yui.actions

import totoro.yui.Yui
import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import kotlin.math.*

// TODO: finish this
@Suppress("unused")
class ChessAction : SensitivityAction("chess") {
    companion object {
        const val WHITE = true
        const val BLACK = false

        const val WHITE_TURN = true
        const val BLACK_TURN = false

        private val ICONS = arrayOf(" ", "♟", "♜", "♞", "♝", "♚", "♛")
        private const val EMPTY = 0
        private const val PAWN = 1
        private const val ROOK = 2
        private const val KNIGHT = 3
        private const val BISHOP = 4
        private const val KING = 5
        private const val QUEEN = 6

        private const val BLACK_TILE_COLOR = F.Black
        private const val WHITE_TILE_COLOR = F.Gray
        private const val BLACK_PIECE_COLOR = F.Red
        private const val WHITE_PIECE_COLOR = F.White

        private const val MAX_CHALLENGES = 500
        private const val MAX_MATCHES = 100
    }

    private val challenges: MutableList<Challenge> = mutableListOf()
    private val matches: MutableList<Match> = mutableListOf()

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        // check if this was a turn
        if (command.args.size >= 2) {
            val turn = Turn.parse(command.args[1])
            if (turn == null) {
                client.send(command.chan, "incorrect syntax for a turn; use something like e2-e4")
            } else {
                val opponent = command.args.first()
                val match = matches.firstOrNull { it.arePlaying(command.user, opponent) }
                if (match == null) {
                    client.send(command.chan, "you are not playing with this user; send a request first (~chess $opponent)")
                } else {
                    if (match.go(turn)) {
                        client.send(command.chan, "\uD83D\uDDF8")
                        client.send(opponent, "it's your turn now in your game with ${command.user}")
                        client.send(match.white, renderBoard(match, !match.turn))
                    } else {
                        client.send(command.chan, "an impossible turn")
                    }
                }
            }
        }
        // check if this was a challenge call or a response to a call
        else if (command.args.isNotEmpty()) {
            val opponent = command.args.first()
            // the target must be in any case online
            if (client.isUserOnline(opponent)) {
                // check if we already have a game in play,
                // because two users cannot be playing more than one game between then at a time
                val activeGame = matches.firstOrNull { it.arePlaying(command.user, opponent) }
                when {
                    // if we have a game in play - print some info
                    activeGame != null -> {
                        client.send(command.chan,
                                "the game already started; ${
                                        if (activeGame.isTurnOf(command.user)) "it's your turn"
                                        else "the opponent is thinking"
                                }"
                        )
                        client.send(command.chan, renderBoardForPlayer(activeGame, command.user))
                    }
                    // if we have an counter call - then start a game
                    challenges.any { it.correspondTo(opponent, command.user) } -> {
                        val match = initMatch(command.user, opponent)
                        if (matches.size > MAX_MATCHES) {
                            matches.removeAt(0)
                            client.send(command.chan, "one of the oldest matches was dropped, because of overflow")
                        }
                        matches.add(match)
                        client.send(command.chan,
                                "challenge was accepted, the game between ${match.white} (white) " +
                                "and ${match.black} (black) started"
                        )
                        client.send(match.white, "it's your turn now in your game with ${match.black}")
                        client.send(match.white, renderBoard(match))

                    }
                    // if we have a double call - then notify the user about that
                    challenges.any { it.correspondTo(command.user, opponent) } -> {
                        client.send(command.chan, "you have already called that user, status: pending")
                    }
                    // and if we don't have anything on the topic - then create a new call
                    else -> {
                        client.send(opponent,
                                "${command.user} challenges you for a game of chess! " +
                                "(answer with ~chess ${command.user})"
                        )
                        client.send(command.chan, "request was sent")
                        if (challenges.size >= MAX_CHALLENGES) {
                            challenges.removeAt(0)
                            client.send(command.chan,
                                    "one of the oldest challenges was dropped, because of the overflow"
                            )
                        }
                        challenges.add(Challenge(command.user, opponent))
                    }
                }
            } else {
                client.send(command.chan, "the nickname seems to be incorrect, or the user is offline")
            }
        }
        // else send the current information summary
        else {
            var info = false

            // check if there are any actual for you challenge calls
            if (challenges.isNotEmpty()) {
                val outgoing = challenges.filter { it.challenger == command.user }.map { it.challengee }
                if (outgoing.isNotEmpty()) {
                    client.send(command.chan, "you have channenged: " + outgoing.joinToString(", "))
                    info = true
                }
                val incoming = challenges.filter { it.challengee == command.user }.map { it.challenger }
                if (incoming.isNotEmpty()) {
                    client.send(command.chan, "you are channenged by: " + incoming.joinToString(", "))
                    info = true
                }
            }

            // check if you are already playing some games
            val matches = matches.filter { it.white == command.user || it.black == command.user }
            if (matches.isNotEmpty()) {
                val opponents = matches.map { if (it.white == command.user) it.black else it.white }
                client.send(command.chan, "you have active match with: " + opponents.joinToString(", "))
                info = true
            }

            if (!info) client.send(command.chan, "challenge someone to play a game (~chess <nickname>)")
        }

        return true
    }

    private fun initMatch(playerA: String, playerB: String): Match {
        val playerAisWhite = Yui.Random.nextBoolean()
        return Match(
                if (playerAisWhite) playerA else playerB,
                if (playerAisWhite) playerB else playerA,
                arrayOf(
                        ROOK, KNIGHT, BISHOP, QUEEN, KING, BISHOP, KNIGHT, ROOK,
                        PAWN, PAWN, PAWN, PAWN, PAWN, PAWN, PAWN, PAWN,
                        EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY,
                        EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY,
                        EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY,
                        EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY,
                        PAWN, PAWN, PAWN, PAWN, PAWN, PAWN, PAWN, PAWN,
                        ROOK, KNIGHT, BISHOP, QUEEN, KING, BISHOP, KNIGHT, ROOK
                ),
                arrayOf(
                        BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK,
                        BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK,
                        BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK,
                        BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK,
                        WHITE, WHITE, WHITE, WHITE, WHITE, WHITE, WHITE, WHITE,
                        WHITE, WHITE, WHITE, WHITE, WHITE, WHITE, WHITE, WHITE,
                        WHITE, WHITE, WHITE, WHITE, WHITE, WHITE, WHITE, WHITE,
                        WHITE, WHITE, WHITE, WHITE, WHITE, WHITE, WHITE, WHITE
                ),
                WHITE_TURN
        )
    }

    private fun getBackGroundColor(i: Int): Boolean {
        return (i / 8 + i) % 2 == 0
    }
    private fun getX(i: Int): Int {
        return i % 8
    }
    private fun getY(i: Int): Int {
        return i / 8
    }

    private fun renderBoardForPlayer(match: Match, player: String): String {
        return (
            if (match.isWhite(player)) renderBoard(match)
            else renderBoard(match, true)
        )
    }
    private fun renderBoard(match: Match, reversed: Boolean = false): String {
        val builder = StringBuilder()

        for (i in (if (!reversed) 0 .. 63 else 63 downTo 0)) {
            val leftBorder = i % 8 == if (reversed) 7 else 0
            val rightBorder = i % 8 == if (reversed) 0 else 7
            val backColor = getBackGroundColor(i)

            if (leftBorder) {
                builder.append((8 - i / 8).toString())
                builder.append(
                        F.mix(
                                if (backColor == BLACK) BLACK_TILE_COLOR else WHITE_TILE_COLOR,
                                WHITE_TILE_COLOR
                        )
                )
                builder.append("▐")
            }

            builder.append(
                    F.mix(
                            if (match.colors[i] == BLACK) BLACK_PIECE_COLOR else WHITE_PIECE_COLOR,
                            if (backColor == BLACK) BLACK_TILE_COLOR else WHITE_TILE_COLOR
                    )
            )
            builder.append(ICONS[match.pieces[i]])

            builder.append(
                    if (rightBorder) {
                        F.mix(
                                if (backColor == BLACK) BLACK_TILE_COLOR else WHITE_TILE_COLOR,
                                WHITE_TILE_COLOR
                        )
                    } else {
                        if (backColor == BLACK) F.mix(BLACK_TILE_COLOR, WHITE_TILE_COLOR)
                        else F.mix(WHITE_TILE_COLOR, BLACK_TILE_COLOR)
                    }
            )
            builder.append("▌")

            if (rightBorder) builder.append(F.Reset + "\n")
        }

        if (reversed) builder.append("  h g f e d c b a \n")
        else builder.append("  a b c d e f g h \n")

        return builder.toString()
    }

    private class Turn(val from: Int, val to: Int, val type: Int, val len: Int) {
        companion object {
            const val UNKNOWN = -1
            const val VERTICAL = 0
            const val HORIZONTAL = 1
            const val FORWARD_DIAGONAL = 2
            const val BACKWARD_DIAGONAL = 3
            const val JUMP = 4
            const val SHORT_CASTLING = 5
            const val LONG_CASTLING = 6

            private const val aCode = 'a'.toInt()
            private const val zeroCode = '0'.toInt() + 1

            fun parse(str: String): Turn? {
                val lower = str.toLowerCase()

                if (lower == "0-0" || lower == "o-o") return Turn(-1, -1, SHORT_CASTLING, -1)
                if (lower == "0-0-0" || lower == "o-o-o") return Turn(-1, -1, LONG_CASTLING, -1)
                if (lower.length < 4) return null

                val fromX = lower[0].toInt() - aCode
                val fromY = 7 - (lower[1].toInt() - zeroCode)
                val toX = lower[lower.length - 2].toInt() - aCode
                val toY = 7 - (lower[lower.length - 1].toInt() - zeroCode)

                val from = fromY * 8 + fromX
                val to = toY * 8 + toX
                val type = detectType(from, to)

                return if (from in 0..63 && to in 0..63 && type != UNKNOWN)
                    Turn(from, to, type, getLen(fromX, fromY, toX, toY))
                else null
            }

            private fun detectType(from: Int, to: Int): Int {
                if (from / 8 == to / 8) return HORIZONTAL
                if (from % 8 == to % 8) return VERTICAL
                val diff = to - from
                if (diff % 9 == 0) return FORWARD_DIAGONAL
                if (diff % 7 == 0) return BACKWARD_DIAGONAL
                val ad = abs(diff)
                if (ad == 6 || ad == 10 || ad == 15 || ad == 17) return JUMP
                return UNKNOWN
            }

            private fun getLen(fromX: Int, fromY: Int, toX: Int, toY: Int): Int {
                return sqrt((toX - fromX).toDouble().pow(2) + (toY - fromY).toDouble().pow(2)).toInt()
            }
        }
    }
    private class Challenge(val challenger: String, val challengee: String) {
        fun correspondTo(challenger: String, challengee: String): Boolean {
            return this.challenger == challenger && this.challengee == challengee
        }
    }
    private class Match(
            val white: String,
            val black: String,
            val pieces: Array<Int>,
            val colors: Array<Boolean>,
            var turn: Boolean
    ) {
        fun isWhite(player: String): Boolean {
            return player == white
        }
        fun isBlack(player: String): Boolean {
            return player == black
        }
        fun isPlaying(player: String): Boolean {
            return player == white || player == black
        }
        fun arePlaying(playerA: String, playerB: String): Boolean {
            return (white == playerA && black == playerB) || (white == playerB && black == playerA)
        }

        fun opponent(player: String): String {
            return if (player == white) black else white
        }

        fun isTurnOf(player: String): Boolean {
            return if (isPlaying(player)) {
                ((player == white && turn == WHITE_TURN) ||
                 (player == black && turn == BLACK_TURN))
            } else false
        }

        private fun checkLine(start: Int, end: Int, step: Int): Boolean {
            for (i in (start + step) .. (end - step) step step) {
                if (pieces[i] != EMPTY) return false
            }
            return true
        }

        fun isTurnUnobstructed(turn: Turn, side: Boolean): Boolean {
            val min = min(turn.from, turn.to)
            val max = max(turn.from, turn.to)
            return when (turn.type) {
                Turn.HORIZONTAL -> checkLine(min, max, 1)
                Turn.VERTICAL -> checkLine(min, max, 8)
                Turn.FORWARD_DIAGONAL -> checkLine(min, max, 9)
                Turn.BACKWARD_DIAGONAL -> checkLine(min, max, 7)
                Turn.SHORT_CASTLING -> when (side) {
                    BLACK_TURN -> checkLine(4, 7, 1)
                    WHITE_TURN -> checkLine(60, 63, 1)
                    else -> false
                }
                Turn.LONG_CASTLING -> when (side) {
                    BLACK_TURN -> checkLine(0, 4, 1)
                    WHITE_TURN -> checkLine(56, 60, 1)
                    else -> false
                }
                Turn.JUMP -> true
                else -> false
            }
        }

        fun go(turn: Turn): Boolean {
            val valid = (
                (this.turn == WHITE_TURN && colors[turn.from] == WHITE) ||
                (this.turn == BLACK_TURN && colors[turn.from] == BLACK)
            ) && (turn.from != turn.to) && isTurnUnobstructed(turn, this.turn) && (
                when (pieces[turn.from]) {
                    EMPTY -> false // because you cannot move nothing
                    ROOK -> {
                        turn.type == Turn.HORIZONTAL || turn.type == Turn.VERTICAL
                    }
                    KNIGHT -> {
                        turn.type == Turn.JUMP
                    }
                    BISHOP -> {
                        turn.type == Turn.FORWARD_DIAGONAL || turn.type == Turn.BACKWARD_DIAGONAL
                    }
                    QUEEN -> {
                        turn.type == Turn.HORIZONTAL || turn.type == Turn.VERTICAL ||
                        turn.type == Turn.FORWARD_DIAGONAL || turn.type == Turn.BACKWARD_DIAGONAL
                    }
                    KING -> {
                        (turn.type == Turn.HORIZONTAL || turn.type == Turn.VERTICAL ||
                         turn.type == Turn.FORWARD_DIAGONAL || turn.type == Turn.BACKWARD_DIAGONAL) &&
                        turn.len == 1
                    }
                    PAWN -> {
                        turn.type == Turn.VERTICAL && (
                            turn.len == 1 || (turn.len == 2 && (
                                if (this.turn == WHITE_TURN) turn.from / 8 == 6 else turn.from / 8 == 1
                            ))
                        )
                        // TODO: add taking rules
                    }
                    else -> false  // must not happen, because there are no more chess pieces in the game
                }
            )
            if (valid) {
                colors[turn.to] = colors[turn.from]
                pieces[turn.to] = pieces[turn.from]
                pieces[turn.from] = EMPTY
                this.turn = !this.turn
            }
            return valid
        }
    }

    override val description = "[WIP]"
}
