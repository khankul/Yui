package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient

@Suppress("unused")
@Action
class CirclifyAction : SensitivityAction("circlify", "circled", "circle",
        "roundify", "rounded", "round", "o") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val combined = command.modifier == "c" || command.modifier == "combine" || command.modifier == "combined"
        if (combined) {
            client.send(command.chan, command.content.map { "$it⃝ " }.joinToString(""))
        } else {
            val squareFlavour = command.modifier == "s" || command.modifier == "square" || command.modifier == "squared"
            client.send(command.chan, round(command.content, squareFlavour))
        }
        return true
    }

    private val rules = hashMapOf(
            '1' to '❶', '2' to '❷', '3' to '❸', '4' to '❹', '5' to '❺', '6' to '❻',
            '7' to '❼', '8' to '❽', '9' to '❾', '0' to '⓿',
            'A' to "🅐", 'B' to "🅑", 'C' to "🅒", 'D' to "🅓", 'E' to "🅔", 'F' to "🅕", 'G' to "🅖",
            'H' to "🅗", 'I' to "🅘", 'J' to "🅙", 'K' to "🅚", 'L' to "🅛", 'M' to "🅜", 'N' to "🅝",
            'O' to "🅞", 'P' to "🅟", 'Q' to "🅠", 'R' to "🅡", 'S' to "🅢", 'T' to "🅣", 'U' to "🅤",
            'V' to "🅥", 'W' to "🅦", 'X' to "🅧", 'Y' to "🅨", 'Z' to "🅩",
            'А' to "🅐", 'Б' to '❻', 'В' to "🅑", 'Г' to '➐', 'Д' to "🅓", 'Е' to "🅔", 'Ё' to "🅔",
            'Ж' to "⓰", 'З' to '❸', 'И' to "🅝", 'Й' to "🅝", 'К' to "🅚", 'Л' to "⓱", 'М' to "🅜",
            'Н' to "🅗", 'О' to "🅞", 'П' to "⓫", 'Р' to "🅟", 'С' to "🅒", 'Т' to "🅣", 'У' to "🅨",
            'Ф' to "🅠", 'Х' to "🅧", 'Ц' to "🅤", 'Ч' to "❹", 'Ш' to "🅦", 'Щ' to "🅦", 'Ъ' to "❻",
            'Ы' to "⓰", 'Ь' to "❻", 'Э' to "❸", 'Ю' to "❿", 'Я' to "🅡"
    )

    private val squareRules = hashMapOf(
            'A' to "🅰", 'B' to "🅱", 'C' to "🅲", 'D' to "🅳", 'E' to "🅴", 'F' to "🅵", 'G' to "🅶",
            'H' to "🅷", 'I' to "🅸", 'J' to "🅹", 'K' to "🅺", 'L' to "🅻", 'M' to "🅼", 'N' to "🅽",
            'O' to "🅾", 'P' to "🅿", 'Q' to "🆀", 'R' to "🆁", 'S' to "🆂", 'T' to "🆃", 'U' to "🆄",
            'V' to "🆅", 'W' to "🆆", 'X' to "🆇", 'Y' to "🆈", 'Z' to "🆉",
            'А' to "🅰", 'В' to "🅱", 'Д' to "🅳", 'Е' to "🅴", 'Ё' to "🅴",
            'И' to "🅽", 'Й' to "🅽", 'К' to "🅺", 'М' to "🅼",
            'Н' to "🅷", 'О' to "🅾", 'Р' to "🅿", 'С' to "🅲", 'Т' to "🆃", 'У' to "🆈",
            'Ф' to "🆀", 'Х' to "🆇", 'Ц' to "🆄", 'Ш' to "🆆", 'Щ' to "🆆",
            'Я' to "🆁"
    )

    private fun round(phrase: String, squareFlavour: Boolean = false): String {
        if (phrase.isEmpty()) return phrase
        return phrase.map {
            when {
                squareFlavour && it in squareRules -> squareRules[it]!!
                squareFlavour && it.toUpperCase() in squareRules -> squareRules[it.toUpperCase()]!!
                it in rules -> rules[it]!!
                it.toUpperCase() in rules -> rules[it.toUpperCase()]
                else -> it
            }
        }.joinToString(" ")
    }

    override val description = "\uD83C\uDD5C \uD83C\uDD50 \uD83C\uDD5A \uD83C\uDD54 \uD83C\uDD62   \uD83C\uDD54 \uD83C" +
            "\uDD65 \uD83C\uDD54 \uD83C\uDD61 \uD83C\uDD68 \uD83C\uDD63 \uD83C\uDD57 \uD83C\uDD58 \uD83C\uDD5D \uD83C" +
            "\uDD56   \uD83C\uDD5B \uD83C\uDD5E \uD83C\uDD5E \uD83C\uDD5A   \uD83C\uDD5B \uD83C\uDD58 \uD83C\uDD5A " +
            "\uD83C\uDD54   \uD83C\uDD63 \uD83C\uDD57 \uD83C\uDD58 \uD83C\uDD62 . \uD83C\uDD7E \uD83C\uDD81   \uD83C" +
            "\uDD83 \uD83C\uDD77 \uD83C\uDD78 \uD83C\uDD82  (with :s or :square modifier). Also there is :combine or :c" +
            ", but not all IRC clients support such thing."
}
