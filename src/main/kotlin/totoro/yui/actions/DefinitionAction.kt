package totoro.yui.actions

import totoro.yui.Config
import totoro.yui.Yui
import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.Language
import totoro.yui.util.LanguageHelper
import totoro.yui.util.StringHelper
import totoro.yui.util.api.Datamuse
import totoro.yui.util.api.DictionaryRu
import totoro.yui.util.api.data.Definition

@Suppress("unused")
@Action
class DefinitionAction : SensitivityAction("def", "define", "definition",
        "whatis", "wtfis", "what", "wtf") {
    private var lastWord = ""
    private var lastDefinitions = mutableListOf<Definition>()

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        return if (command.args.isNotEmpty()) {
            val args = if (command.args.size > 1 && (command.name == "wtf" || command.name == "what") && command.args[0] == "is")
                command.args.drop(1)
            else
                command.args
            val partOfSpeech = args.filter { it.startsWith("(") && it.endsWith(")") }.map { it.drop(1).dropLast(1) }
            val filteredArgs = args.filterNot { it.startsWith("(") && it.endsWith(")") }
            val word = filteredArgs.joinToString(" ")

            if (word != lastWord) {
                lastDefinitions =
                        if (LanguageHelper.detect(word) == Language.ENGLISH) Datamuse.definitions(word).toMutableList()
                        else DictionaryRu.definitions(word).toMutableList()
                lastWord = word
            }

            val filteredDefinitions =
                    if (partOfSpeech.isEmpty()) lastDefinitions
                    else lastDefinitions.filter { it.partOfSpeech.isEmpty() || partOfSpeech.contains(it.partOfSpeech) }

            when {
                filteredDefinitions.isNotEmpty() -> {
                    val def = filteredDefinitions[Yui.Random.nextInt(filteredDefinitions.size)]
                    val message = if (def.partOfSpeech.isBlank())
                        F.Yellow + def.word + F.Reset + ": " + F.Italic + def.definition
                    else
                        F.Yellow + def.word + F.Reset + ": " + "${def.partOfSpeech}  " + F.Italic + def.definition
                    client.send(command.chan, StringHelper.utf8trim(message,
                            Config.maxMessageLength - 4 - command.chan.length - client.getMessagePrefixLength(), true) + F.Reset)
                    lastDefinitions.remove(def)
                }
                lastWord == word ->
                    client.send(command.chan, "no more definitions found")
                else ->
                    client.send(command.chan, "no definitions found")
            }
            true
        } else false
    }

    override val description = "English and Russian dictionary. Shows a definition for the given word / phrase. " +
            "Consecutive calls will get all available definitions, one by one. Example: ~what is love. You can specify " +
            "preferred part of speech: ~def love (n)"
}
