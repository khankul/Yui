package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Dict

@Suppress("unused")
@Action
class DiceAction : SensitivityAction("dice") {
    private val maxAmount = 50
    private val faces = Dict.of("⚀", "⚁", "⚂", "⚃", "⚄", "⚅")

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val amount = if (command.args.isNotEmpty()) {
            command.args.first().toIntOrNull() ?: 1
        } else 1
        client.send(command.chan, (1..amount.coerceAtMost(maxAmount)).joinToString(" ") { faces() })
        return true
    }

    override val description = "Fair dice roll. You can specify the number of dices, but no more than $maxAmount."
}
