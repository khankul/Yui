package totoro.yui.actions

/**
 * This interface provides some help, some explanation of the subject.
 * The explanation comes in text form (String).
 */
interface Explained {
    fun explanation(): String
}
