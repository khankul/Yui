package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import java.math.BigInteger
import java.nio.charset.StandardCharsets
import java.security.MessageDigest

@Suppress("unused")
@Action
class HashAction : SensitivityAction("hash", "md5", "sha1", "sha-1", "sha256", "sha-256") {
    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        when (if (command.name == "hash") command.modifier else command.name) {
            "md5" -> client.send(command.chan, hash("MD5", command.content))
            "sha-1", "sha1" -> client.send(command.chan, hash("SHA-1", command.content))
            "sha-256", "sha256" -> client.send(command.chan, hash("SHA-256", command.content))
            else -> client.send(command.chan, "this algorithm is not supported yet")
        }
        return true
    }

    private fun hash(algorithm: String, value: String): String {
        val md5digest = MessageDigest.getInstance(algorithm)
        md5digest.update(value.toByteArray(StandardCharsets.UTF_8))
        val raw = md5digest.digest()
        val integer = BigInteger(1, raw)
        return integer.toString(16)
    }

    override val description = "Hash calculator. Supports MD5, SHA1 and SHA-256. Select one by using modifier or a " +
            "command alias. Example: ~hash:md5 internet"
}
