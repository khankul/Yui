package totoro.yui.actions

import totoro.yui.actions.data.Priority
import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.db.Database
import totoro.yui.db.Hook
import totoro.yui.util.Dict
import totoro.yui.util.F

/**
 * Allows user to `hook` the target.
 * When the target writes any message to the channel, the user will receive a notification.
 */

class HookAction(private val database: Database) : SensitivityAction("hook", "unhook", "send", "give", "tell"), MessageAction {

    companion object {
        private var action: HookAction? = null
        fun instance(database: Database): HookAction {
            if (action == null) action = HookAction(database)
            return action!!
        }
    }

    override val priority = Priority.PROXIMATE

    private val confirmations = Dict.AcceptTask + Dict.of("ok, i'll watch for him", "i'll notify you")

    private val nicknameRegex = "^[a-zA-Z_\\-\\[\\]\\\\^{}|`][a-zA-Z0-9_\\-\\[\\]\\\\^{}|`]*$".toRegex()
    private val noTarget = Dict.of(
        "${F.Red}wait, wait, wait a minute!${F.Reset} who is the target?",
        "${F.Red}no-no-no${F.Reset}, please give me a nickname!",
        "${F.Red}stop right there!${F.Reset} who is this message for?"
    )

    private fun add(user: String, target: String, message: String?) =
            database.hooks?.add(Hook(0, user, target, message ?: "")) != null

    private fun remove(user: String, target: String) =
            database.hooks?.removeHooks(user, target)

    private fun triggerHooks(client: IRCClient, channel: String, user: String): Boolean {
        val hooks = database.hooks?.pullAllHooksFor(user)
        return if (hooks != null && hooks.isNotEmpty()) {
            hooks.forEach {
                if (it.text.isEmpty())
                    client.send(channel, "${it.hunter}: ${F.Yellow}alarm! <$user> is here!${F.Reset}")
                else
                    client.send(channel, "<${it.hunter}> to <$user>: ${F.Yellow}${it.text}${F.Reset}")
            }
            true
        } else false
    }

    override fun processMessage(client: IRCClient, channel: String, user: String, message: String): String? {
        return if (triggerHooks(client, channel, user)) null else message
    }

    override fun willProcess(client: IRCClient, command: Command): Boolean {
        triggerHooks(client, command.chan, command.user)
        return super.willProcess(client, command)
    }

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        if (command.args.isNotEmpty()) {
            if (command.name != "unhook") {
                val target = command.args.first()
                if (!target.matches(nicknameRegex)) {
                    client.send(command.chan, noTarget())
                } else {
                    add(command.user, target, command.content.dropWhile { !it.isWhitespace() }.trim())
                    client.send(command.chan, confirmations())
                }
            } else {
                command.args.forEach {
                    remove(command.user, it)
                }
                client.send(command.chan, Dict.AcceptTask())
            }
        } else {
            client.send(command.chan, "whom do you want me to ${command.name}?")
        }
        return true
    }

    override val description = "Simple tracking and message delivery device. Get notification when someone shows " +
            "up in the channel: ~hook someone. Send someone a message (you both will be highlighted): ~send someone message"
}
