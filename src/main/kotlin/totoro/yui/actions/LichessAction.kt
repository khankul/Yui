package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.db.Database
import totoro.yui.util.F
import totoro.yui.util.api.Lichess
import totoro.yui.util.api.data.LichessUser

@Suppress("unused")
class LichessAction(private val database: Database) : SensitivityAction("lichess", "chess", "li", "lc") {
    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val showProvisional = command.modifier == "?"
        val filter: String? = if (showProvisional) null else command.modifier
        val user = if (command.args.isNotEmpty()) {
            val first = command.args.first()
            database.lichess?.set(command.user, first)
            first
        } else {
            database.lichess?.get(command.user)
        }
        if (user != null) {
            Lichess.user(user,
                { client.send(command.chan, formatOutput(it, showProvisional, filter)) },
                { client.send(command.chan, "i have no info about that") })
        } else {
            client.send(command.chan, "i don't know your lichess nickname")
        }
        return true
    }

    // ● UnicornFreedom / 1555 blitz (▴12) / played for 8d 12h 1m
    private fun formatOutput(user: LichessUser, showProvisional: Boolean, filter: String?): String {
        val output = StringBuilder("${if (user.online) F.Green else ""}● ${F.Yellow}${user.username}${F.Reset} / ")
        val provisionalPerfs = if (showProvisional || filter != null) user.perfs else user.perfs.filter { !it.provisional }
        val filteredPerfs = if (filter == null) provisionalPerfs else provisionalPerfs.filter { it.title == filter }
        if (filteredPerfs.isEmpty()) {
            output.append("no data for this filter / ")
        } else {
            filteredPerfs.map { perf ->
                output.append("${F.Yellow}${perf.rating}${F.Reset}${if (perf.provisional) "?" else ""} ${perf.title} ${
                when {
                    perf.progress > 0 -> "(${F.Green}▴${F.Reset}${perf.progress})"
                    perf.progress < 0 -> "(${F.Red}▾${F.Reset}${-perf.progress})"
                    else -> ""
                }
                } / ")
            }
        }
        val totalMinutes: Int = user.playTime / 60
        val minutes: Int = Math.floorMod(totalMinutes, 60)
        val days: Int = Math.floorDiv(totalMinutes, 60 * 24)
        val hours: Int = Math.floorDiv(totalMinutes - minutes - days * 60 * 24, 60)
        if (days > 0 || hours > 0 || minutes > 0) {
            output.append("played for${
            if (days > 0) " ${days}d" else ""
            }${
            if (hours > 0) " ${hours}h" else ""
            }${
            if (minutes > 0) " ${minutes}m" else ""
            }")
        } else {
            output.append("never played chess")
        }
        return output.toString()
    }

    override val description = "Gets user stats from lichess.org. Use :? modifier to show provisional values, or " +
            ":<variant> to filter results by game variant / time control. Example: ~lichess:blitz UnicornFreedom"
}
