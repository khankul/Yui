package totoro.yui.actions

import totoro.yui.Config
import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.StringHelper
import totoro.yui.util.api.Lua

@Suppress("unused")
@Action
class LuaAction : SensitivityAction("lua", "l", "calc", "c") {

    private val maxOutputLines = 3
    private val errorRegex = "^input:\\d*:\\s.*$".toRegex()

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val code = when (command.name) {
            "calc", "c" -> "=(${command.content})"
            else -> command.content
        }
        val result = Lua.run(code)
        if (result == null)
            client.send(command.chan, "something went wrong")
        else {
            val lines = result.split('\n')
            lines.take(maxOutputLines).forEach { line ->
                val message = StringHelper.utf8trim(line,
                        Config.maxMessageLength - 14 - command.chan.length - client.getMessagePrefixLength(), true)
                client.send(command.chan, "lua> ${if (message.matches(errorRegex)) F.Red else F.Yellow}$message${F.Reset}")
            }
            if (lines.size > maxOutputLines)
                client.send(command.chan, "${F.Yellow}(and ${lines.size - maxOutputLines} more...)${F.Reset}")
        }

        return true
    }

    override val description = "Lua interpreter. If you use ~c or ~calc alias everything is wrapped into =(...). Example: ~c 2 + 2"
}
