package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient

@Suppress("unused")
@Action
class MisdirectedAction : SensitivityAction("py", "python", "wa", "tr", "tell", "mc") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        if (client.isBroteOnline()) {
            client.send(command.chan, "maybe try .${command.name} (with the dot)")
            return true
        } else {
            return when (command.name) {
                "py", "python", "wa" -> {
                    client.send(command.chan, "i do not have such command, but you can use ~lua or ~c for calculations")
                    true
                }
                "tell" -> {
                    client.send(command.chan, "brote is offline right now, but you can use ~hook")
                    true
                }
                else -> false
            }
        }
    }

    override val description = "This command is better handled by my colleague brote."
}
