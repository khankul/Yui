package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Dict

@Suppress("unused")
@Action
class NeverGonnaAction : SensitivityAction("never", "nevergonna") {

    private val promises = Dict.of(
            "... give you up",
            "... let you down",
            "... run around",
            "... desert you",
            "... make you cry",
            "... say goodbye",
            "... tell a lie",
            "... hurt you")

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        if (command.name == "nevergonna" || (command.args.size == 1 && command.args[0] == "gonna")) {
            client.send(command.chan, promises())
            return true
        }
        return false
    }

    override val description = "Never gonna give you up, never gonna let you down..."
}
