package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.api.Datamuse

@Suppress("unused")
@Action
class PhoneticsAction : SensitivityAction("say", "pronounce", "phonetics",
        "pronunciation", "transcription", "transcript") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        if (command.args.isNotEmpty()) {
            val phonetics = command.args.mapNotNull { Datamuse.phonetics(it) }
            if (phonetics.isNotEmpty())
                client.send(command.chan, F.Yellow + phonetics.joinToString(" ") { it.word } + F.Reset + ": " +
                        "/${ phonetics.joinToString(" ") { it.phonetics } }/")
            else client.send(command.chan, "cannot find any information about pronunciation")
        } else {
            client.send(command.chan, "gimme a word")
        }
        return true
    }

    override val description = "Shows how the given English word must sound."
}
