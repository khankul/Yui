package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F

@Suppress("unused")
@Action
class PlasmaAction : SensitivityAction("plasma", "plazma") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val iter = command.content.codePoints().iterator()
        val payload = StringBuilder()

        payload.append(F.Bold)

        while (iter.hasNext()) {
            payload.append(F.mix(F.White, F.randomFancyColor()))
            payload.append(Character.toChars(iter.nextInt()))
        }

        payload.append(F.Reset)

        client.send(command.chan, payload.toString())
        return true
    }

    override val description = "More colorful reality filter."
}
