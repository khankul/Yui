package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.Language
import totoro.yui.util.LanguageHelper
import totoro.yui.util.RandomIterator
import totoro.yui.util.api.Datamuse
import totoro.yui.util.api.Rifmus

@Suppress("unused")
@Action
class RhymeAction : SensitivityAction("rhyme") {

    private val maxRhymes = 10

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        return if (command.args.isNotEmpty()) {
            val word = command.args.joinToString(" ")
            val rhymes =
                if (LanguageHelper.detect(word) == Language.ENGLISH) Datamuse.rhyme(word)
                else Rifmus.rhyme(word)
            if (rhymes.isNotEmpty()) {
                client.send(command.chan, F.Yellow + word + F.Reset + ": " +
                        RandomIterator(rhymes).take(maxRhymes).joinToString(", "))
            } else client.send(command.chan, "no rhymes found")
            true
        } else false
    }

    override val description = "Tries its best to rhyme whatever you gave it. Supports English and Russian."
}
