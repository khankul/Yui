package totoro.yui.actions

import totoro.yui.Yui
import totoro.yui.client.Command
import totoro.yui.client.IRCClient

@Suppress("unused")
@Action
class RollAction : SensitivityAction("roll", "roulette", "shoot") {

    private val drumSize = 6
    private var position: Int  = Yui.Random.nextInt(drumSize)

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        if (position > 0) {
            client.send(command.chan, "miss")
            position--
        } else {
            client.send(command.chan, "BANG!")
            position = Yui.Random.nextInt(drumSize)
        }
        return true
    }

    override val description = "Russian roulette. Test your luck."
}
