package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Japanese

@Suppress("unused")
@Action
class RomanizeAction : SensitivityAction("romaji", "romanize", "ro", "rom", "jp", "ja", "jpn", "japan", "japanese") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        if (command.content.isNotEmpty()) {
            client.send(command.chan, Japanese.romanize(command.content).toLowerCase())
        } else {
            client.send(command.chan, "i'm listening")
        }
        return true
    }

    override val description = "Converts kanji and kana (katakana, hiragana) to romaji."
}
