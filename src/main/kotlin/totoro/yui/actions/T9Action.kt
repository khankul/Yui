package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.api.YandexSpeller

@Suppress("unused")
@Action
class T9Action : SensitivityAction("*", "spellcheck", "grammar", "correct", "t9") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        // phrase will be taken from history, if no arguments were given,
        // or the only argument is the nickname of some user
        // otherwise the content of the command will be taken as the phrase
        val phrase = if (command.args.size > 1 || (command.args.size == 1 && !client.isUserOnline(command.args.first())))
            command.content
        else {
            val nickname = if (command.args.isNotEmpty()) command.args.first() else command.user
            client.history.lastByUser(command.chan, nickname)?.message
        }
        val text = phrase?.let { YandexSpeller.correct(it) }
                ?: "i do not remember, what i need to correct?"
        client.send(command.chan, text)
        return true
    }

    override val description = "T9-like corrector. Powered by Yandex. With no args it will take the last line from chat. " +
            "With someone's nickname as a first argument - it will take the last phrase from that user. Or you can just " +
            "provide the phrase instead."
}
