package totoro.yui.actions

import totoro.yui.Config
import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.api.Title

/**
 * Searches for URL addresses in the message and then prints the titles
 * of corresponding web pages
 */

@Suppress("unused")
@Action
class TitleAction : SensitivityAction("title"), MessageAction {

    private val urlRegex = ".*https?://.*\\..*".toRegex()

    private fun printAllTitles(phrases: List<String>, channel: String, client: IRCClient): Boolean {
        val titles = phrases.filter { it.matches(urlRegex) }.mapNotNull { Title.get(it) }
        titles.forEach { client.send(channel, "${F.Yellow}[ $it ]${F.Reset}") }
        return titles.isNotEmpty()
    }

    override fun processMessage(client: IRCClient, channel: String, user: String, message: String): String? {
        return if (!client.isBroteOnline() && message.contains("http") && !Config.blackUsers.contains(user)) {
            if (printAllTitles(message.split(' '), channel, client)) null
            else message
        } else message
    }

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        when {
            command.args.isNotEmpty() && command.args.first().startsWith('^') -> {
                val arg = command.args.first()
                val index = if (arg.endsWith('^')) arg.length
                else arg.drop(1).toIntOrNull()

                if (index != null && index <= client.history.size) {
                    val record = client.history.getFromEnd(command.chan, index - 1) { it.matches(urlRegex) }
                    if (record != null) {
                        if (!printAllTitles(record.message.split(" "), command.chan, client))
                            client.send(command.chan, "i don't see any URLs there")
                    } else {
                        client.send(command.chan, "i do not remember the chan log so far back")
                    }
                } else if (index != null && index > client.history.size(command.chan)) {
                    client.send(command.chan, "i do not remember more than ${client.history.size(command.chan)} messages from this channel")
                } else {
                    client.send(command.chan, "you are doing it wrong")
                }
            }
            command.args.isNotEmpty() ->
                printAllTitles(command.args, command.chan, client)
        }
        return true
    }

    override val description = "Tries to get a webpage title for the given URL. You can use arrows to point to the " +
            "URL, like: ~title ^^ or ~title ^2"
}
