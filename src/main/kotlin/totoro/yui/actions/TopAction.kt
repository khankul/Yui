package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.db.Database
import totoro.yui.util.Dict
import totoro.yui.util.MathHelper
import kotlin.math.max

class TopAction(private val database: Database) : SensitivityAction("top", "pop", "popularity") {

    private val maxCommands = 5

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        if (command.content == "drop" || command.content == "reset") {
            database.stats?.truncate()
            client.send(command.chan, Dict.Success())
        } else {
            val n = max(1, command.args.firstOrNull()?.toIntOrNull() ?: maxCommands)
            val top = database.stats?.getTop(n)

            if (top != null && top.isNotEmpty()) {
                val offset = MathHelper.numberOfDigits(top.first().value)
                top.forEach {
                    client.send(command.chan, "%${offset}d".format(it.value) + " : " + it.name)
                }
            } else {
                client.send(command.chan, "i have no data on the topic")
            }
        }
        return true
    }

    override val description = "Prints a rating of most often used commands. Use :drop or :reset modifier to erase " +
            "collected statistics."
}
