package totoro.yui.actions

import totoro.yui.actions.data.Priority
import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Dict

@Suppress("unused")
@Action
class UnknownAction: CommandAction {
    private val name = "unknown_action"

    override var blocked = false
    override var adminsOnly = false
    /**
     * This action must be always the last one in the processing queue
     */
    override val priority = Priority.ULTIMATE

    override fun willProcess(client: IRCClient, command: Command): Boolean {
        return true
    }

    override fun processCommand(client: IRCClient, command: Command): Command? {
        client.send(command.chan, Dict.NotSure())
        return null
    }

    override fun correspondsName(name: String?): Boolean {
        return name == this.name
    }

    override fun name(): String {
        return name
    }

    override fun explanation(): String {
        return "No explanation for this."
    }
}
