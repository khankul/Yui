package totoro.yui.actions

import org.jetbrains.kotlin.backend.common.pop
import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.api.UrbanDictionary
import totoro.yui.util.api.data.UrbanDefinition

@Suppress("unused")
@Action
class UrbanAction : SensitivityAction("urban", "slang", "ud", "udef") {
    private var lastRequest = ""
    private val lastDefinitions = mutableListOf<UrbanDefinition>()

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val request = command.content
        if (lastRequest == request) {
            if (lastDefinitions.isEmpty())
                client.send(command.chan, "no more definitions found")
            else {
                printOneDefinition(client, command.chan)
            }
        } else {
            UrbanDictionary.definitions(request) { result ->
                if (result != null && result.isNotEmpty()) {
                    lastRequest = request
                    lastDefinitions.clear()
                    lastDefinitions.addAll(result)
                    printOneDefinition(client, command.chan)
                } else {
                    client.send(command.chan, "can not find anything")
                }
            }
        }
        return true
    }

    private fun printOneDefinition(client: IRCClient, channel: String) {
        val definition = lastDefinitions.pop()
        client.send(channel, F.Yellow + definition.word + F.Reset +
                ": (" + F.Green + definition.thumbsUp + F.Reset + "/" + F.Red + definition.thumbsDown + F.Reset + ") " +
                definition.definition)
        if (definition.example != null) {
            client.send(channel, definition.example)
        }
    }

    override val description = "Urban Dictionary search. Can be NSFW. Consecutive calls will fetch alternative " +
            "definitions, one by one."
}
