package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.db.Database
import totoro.yui.util.F
import totoro.yui.util.api.Geo
import totoro.yui.util.api.Wttr

class WeatherAction(private val database: Database) : SensitivityAction("weather", "wttr") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        if (command.content.isEmpty()) {
            val databaseLocation = database.locations?.get(command.user)
            if (databaseLocation == null) {
                client.getUserHost(command.user)?.let { host ->
                    Geo.locationByHost(host) {
                        showWeather("${it?.city ?: ""} ${it?.country ?: ""}", client, command)
                    }
                } ?: showWeather("", client, command)
            } else showWeather(databaseLocation, client, command)
        } else {
            database.locations?.set(command.user, command.content)
            showWeather(command.content, client, command)
        }
        return true
    }

    private fun showWeather(location: String, client: IRCClient, command: Command) {
        Wttr.get(location, { weather ->
            client.send(command.chan, "${weather.location}: " +
                    "${F.Yellow}${weather.condition}${F.Reset} / " +
                    "${F.Yellow}${weather.temperature}${F.Reset} / " +
                    "${F.Yellow}${weather.wind}${F.Reset} / " +
                    "${F.Yellow}${weather.visibility}${F.Reset} / " +
                    "${F.Yellow}${weather.precipitation}${F.Reset}")
        }, {
            client.send(command.chan, "the weather is unclear")
        })
    }

    override val description = "Gets weather info for the location. Remembers the last requested location " +
            "and will use it if no arguments provided."
}
