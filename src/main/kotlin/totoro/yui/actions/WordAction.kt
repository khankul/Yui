package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.api.Datamuse

@Suppress("unused")
@Action
class WordAction : SensitivityAction("guess") {

    private val maxWords = 10

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        return if (command.args.isNotEmpty()) {
            val definition = command.args.joinToString(" ")
            val words = Datamuse.word(definition)
            if (words.isNotEmpty())
                client.send(command.chan, F.Yellow + definition + F.Reset + ": " +
                        words.take(maxWords).joinToString(", "))
            else client.send(command.chan, "no such words found")
            true
        } else false
    }

    override val description = "Tries to guess the word by description."
}
