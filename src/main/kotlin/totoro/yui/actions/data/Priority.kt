package totoro.yui.actions.data

/**
 * This parameter indicates in which order action processors will be called.
 * (PROXIMATE will be called first, INTERMEDIATE is common mode, PENULTIMATE - before ultimate, ULTIMATE - the very last)
 */

enum class Priority {
    PROXIMATE, INTERMEDIATE, PENULTIMATE, ULTIMATE
}
