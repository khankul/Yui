package totoro.yui.db

import java.sql.Connection

class HooksTable(connection: Connection) : Table(connection, "hooks") {

    fun init() {
        update("create table if not exists $table (hunter string, target string, text string);")
    }

    fun add(hook: Hook): Long? {
        // escape single quotes
        val hunter = hook.hunter.replace("'", "")
        val target = hook.target.replace("'", "")
        val text = hook.text.replace("'", "''")
        // execute select query
        return insert("insert into $table values('$hunter', '$target', '$text');").getOrNull(0)
    }

    fun pullAllHooksFor(nickname: String): List<Hook>? {
        val target = nickname.replace("'", "")
        val result = query("select rowid, * from $table where target = '$target';")
        var hooks: MutableList<Hook>? = null
        while(result.next()) {
            if (hooks == null) hooks = mutableListOf()
            hooks.add(Hook(
                    result.getLong(1),
                    result.getString("hunter"),
                    result.getString("target"),
                    result.getString("text")
            ))
        }
        if (hooks != null && hooks.isNotEmpty())
            update("delete from $table where target = '$target';").close()
        return hooks
    }

    fun removeHooks(from: String, to: String) {
        val hunter = from.replace("'", "")
        val target = to.replace("'", "")
        update("delete from $table where hunter = '$hunter' and target = '$target';").close()
    }
}
