package totoro.yui.db

import java.sql.Connection

class LichessTable(connection: Connection) : PersonalRecordTable(connection, "lichess_username")
