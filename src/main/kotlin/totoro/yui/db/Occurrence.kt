package totoro.yui.db

data class Occurrence(val id: Long, val chain: String, val word: String, val author: String)
