package totoro.yui.db

import java.sql.Connection
import java.sql.ResultSet

class QuotesTable(connection: Connection) : Table(connection, "quotes") {

    fun init() {
        update("create table if not exists $table (text string);")
    }

    fun add(quote: Quote): Long? {
        // escape single quotes
        val text = quote.text.replace("'", "''")
        // execute select query
        return insert("insert into $table values('$text');").getOrNull(0)
    }

    fun random(): Quote? {
        return firstFrom(query("select rowid, * from $table order by random() limit 1;"))
    }

    fun get(id: Long): Quote? {
        return firstFrom(query("select rowid, * from $table where rowid = $id;"))
    }

    private fun firstFrom(result: ResultSet): Quote? {
        var quote: Quote? = null
        if (result.next()) {
            quote = Quote(
                result.getLong(1),
                result.getString("text")
            )
        }
        result.close()
        return quote
    }
}
