package totoro.yui.util

object NameGen {
    private val consonants = Dict.of(
            'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n','p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z'
    )
    private val vowels = Dict.of('a', 'e', 'i', 'o', 'u')
    private val syllables = Dict.of(
            arrayOf(true, false),
            arrayOf(true, false, true),
            arrayOf(false, true),
            arrayOf(false, true, false),
            arrayOf(false),
            arrayOf(true, true, false),
            arrayOf(true, false, false)
    )
    private const val maxSyllables = 5

    fun name(): String {
        return name((1..maxSyllables).random())
    }

    fun name(len: Int): String {
        val builder = StringBuilder()
        for (i in 1..len) {
            val pattern = syllables()
            pattern.forEach {
                if (it) builder.append(consonants())
                else builder.append(vowels())
            }
        }
        return builder.toString()
    }
}
