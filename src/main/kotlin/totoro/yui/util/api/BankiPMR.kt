package totoro.yui.util.api

import org.jsoup.Jsoup

object BankiPMR {
    private const val useragent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) " +
            "Chrome/59.0.3071.115 Safari/537.36"

    fun rate(): Double? {
        return try {
            val doc = Jsoup.connect("https://bankipmr.ru/").userAgent(useragent).get()
            val cell = doc.select("td[aria-label='Доллар (USD)']").firstOrNull()
            cell?.text()?.toDouble()
        } catch (e: Exception) {
            null
        }
    }
}
