package totoro.yui.util.api

import org.jsoup.Connection
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import totoro.yui.Log
import java.io.IOException
import java.net.URLDecoder
import java.net.URLEncoder

object Google {
    private const val charset = "UTF-8"
    private const val useragent = "Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0"

    private val cookies: MutableMap<String, String> = mutableMapOf()

    private fun url(request: String): String {
        return "https://www.google.com/search?ie=utf-8&hl=en&source=hp&gbv=1" +
                "&q=$request&safe=disabled"
    }

    fun downloadDocument(url: String): Document? {
        val connect = Jsoup.connect(url)
            .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
            .header("Accept-Language", "en-US,en;q=0.5")
            .header("Accept-Encoding", "gzip, deflate")
            .header("DNT", "1")
            .header("Connection", "keep-alive")
            .header("Upgrade-Insecure-Requests", "1")
            .userAgent(useragent)

        var response = connect.cookies(cookies).method(Connection.Method.GET).execute()
        if (cookies.isEmpty()) {
            cookies.putAll(response.cookies())
            Log.info("Google API: Successfully got cookies for this session...")
        }
        var document = response.parse()

        if (
            document.title().contains("Before you continue to") ||
            document.title().contains("Bevor Sie zu") ||
            document.title().contains("Než budete pokračovat na")
        ) {
            Log.info("Google API: Bypassing a cookie banner...")
            val form = document.selectFirst("form")
            val action = form.attr("action")
            val data = form.select("input[type=\"hidden\"]").associate { input ->
                input.attr("name") to input.attr("value")
            }
            try {
                response =
                    Jsoup.connect(action).data(data).cookies(cookies).method(Connection.Method.POST).execute()
                cookies.clear()
                cookies.putAll(response.cookies())
                document = response.parse()
            } catch (e: IOException) {
                Log.warn("Google API: Cookie banner bypassing failed!")
                return null
            }
        }

        return document
    }

    fun search(request: String): Pair<String, String>? {
        val document = downloadDocument(url(URLEncoder.encode(request, charset)))
        val links = document?.select(".kCrYT>a")
        return if (links != null && links.isNotEmpty()) {
            val first = links.first { it.attr("href").startsWith("/url?") }
            val title = first.getElementsByClass("vvjwJb").text()
            val rawUrl = first.absUrl("href")
            val url = URLDecoder.decode(rawUrl, charset)
            Pair(url.substring(url.indexOf('=') + 1, url.indexOf("&sa")), title)
        } else null
    }
}
