package totoro.yui.util.api

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result

object Pastebin {
    fun post(content: String, success: (String) -> Unit, failure: () -> Unit) {
        "https://hastebin.com/documents".httpPost().body(content).responseString { _, _, result ->
            when (result) {
                is Result.Failure -> failure()
                is Result.Success -> {
                    val json = Parser.default().parse(StringBuilder(result.value)) as JsonObject
                    val key = json.string("key")
                    if (key != null) success(key)
                    else failure()
                }
            }
        }
    }
}
