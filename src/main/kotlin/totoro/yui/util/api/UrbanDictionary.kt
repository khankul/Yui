package totoro.yui.util.api

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import totoro.yui.util.api.data.UrbanDefinition

object UrbanDictionary {
    fun definitions(word: String, handler: (List<UrbanDefinition>?) -> Unit) {
        "http://api.urbandictionary.com/v0/define?term=$word".httpGet().responseString { _, _, result ->
            when (result) {
                is Result.Failure -> handler(null)
                is Result.Success -> {
                    val json = Parser.default().parse(StringBuilder(result.value)) as JsonObject
                    val list = json.array<JsonObject>("list")
                    handler(list?.mapNotNull { item ->
                        val correctWord = item.string("word")
                        val thumbsUp = item.int("thumbs_up")
                        val thumbsDown = item.int("thumbs_down")
                        val definition = item.string("definition")
                        val example = item.string("example")
                        if (correctWord != null && definition != null) {
                            UrbanDefinition(correctWord, thumbsUp ?: 0, thumbsDown ?: 0, definition, example)
                        } else null
                    })
                }
            }
        }
    }
}
