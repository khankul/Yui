package totoro.yui.util.api.data

data class Changes(val content: List<String>, val trimmed: Boolean)
