package totoro.yui.util.api.data

class Geolocation(val country: String?, val city: String?, val provider: String?, val timezone: String?)
