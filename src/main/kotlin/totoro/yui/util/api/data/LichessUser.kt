package totoro.yui.util.api.data

@Suppress("unused")
class LichessUser (
    val username: String,
    val online: Boolean,
    val perfs: List<LichessPerf>,
    val playTime: Int
)
