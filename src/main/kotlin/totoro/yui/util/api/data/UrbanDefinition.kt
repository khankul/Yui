package totoro.yui.util.api.data

class UrbanDefinition(
        val word: String,
        val thumbsUp: Int, val thumbsDown: Int,
        val definition: String,
        val example: String?
)
